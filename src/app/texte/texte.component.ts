import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TextService } from '../_services/text.service';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-texte',
  templateUrl: './texte.component.html',
  styleUrls: ['./texte.component.css']
})
export class TexteComponent implements OnInit {

  constructor(public textSrv:TextService,public alertService:AlertService) { }
  listText:any;
  submitted=false;
  statutAdminPlat:string;
  ajout:boolean;
  ngOnInit(): void {
    this.ajout = true;
    this.statutAdminPlat = 'Ajouter'
    this.textSrv.getAll()
    .then(data => {
      this.listText = data
      console.log(this.listText);
    })

  }
  text = new FormGroup({
    section: new FormControl(''),
    id:new FormControl(''),
    text: new FormControl(''),
    // type: new FormControl(''),
  });
  onUpdate(id) {
    this.ajout = false;
    this.statutAdminPlat = 'Modifier'
    console.log(id);
    console.log(this.listText);
    this.listText.forEach(element => {
      if (element._id === id) {
        console.log(element);
        this.text = new FormGroup({
          section: new FormControl(element.section),
          text: new FormControl(element.text),
          id: new FormControl(element._id),
          // type: new FormControl(''),
          
        });

      }
    });

    
  }
  onSubmit(){
    if (this.ajout) {
      console.log(this.text.value);
      this.textSrv.add(this.text.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    } else {
      console.log('on update')
      this.textSrv.update(this.text.value).then(res => {
        console.log('error',res)
          if (res.status == '500' || res.status == '401') {
              console.log('error')
              this.submitted = false;
              this.alertService.error('erreur');
          } else {
              this.submitted = true;
              this.alertService.success('OK');
              this.initForm()
          }
        });
    }
    
    // console.warn(this.text.value);
    
    
  }
  initForm(){
    this.text = new FormGroup({
      section: new FormControl(''),
      id:new FormControl(''),
      text: new FormControl(''),
      // type: new FormControl(''),
    });
    // ici on reinitialise la liste de checkbox 


    this.ngOnInit();
  }
}
