﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../_services/auth.service';
import { AlertService } from '../_services/alert.service';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
 })
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private alertService: AlertService,
        public authService: AuthService) { }

    ngOnInit() {
        localStorage.removeItem('currentUser');

        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        if (localStorage.getItem('currentUser')) {
            let returnUrl = '/index';
                if (this.returnUrl) {
                  returnUrl = this.returnUrl;
                }
                this.router.navigate([returnUrl]);
            }
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    tryLogin() {
        console.log('try login')
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        const value = {
            email: this.f.email.value,
            password: this.f.password.value
        };
        this.authService.doLogin(value)
            .then(res => {
                // console.log('error',res)
                if (res.status == '500' || res.status == '401') {
                    console.log('error')
                    this.submitted = false;
                    this.alertService.error('erreur lors de la connexion , veuillez réessayer');
                } else {
                    console.log('là ca fonctionne on peut setuserinstorage')
                    console.log(res);
                    this.setUserInStorage(res);
                    localStorage.removeItem('currentLayoutStyle');
                    let returnUrl = '/dashboard/sales';
                    
                    if (this.returnUrl) {
                    returnUrl = this.returnUrl;
                    }
                    this.router.navigate([returnUrl]);
                }
                
            });
    }

    setUserInStorage(res) {
        if (res.user) {
            localStorage.setItem('currentUser', JSON.stringify(res.user));
        } else {
            localStorage.setItem('currentUser', JSON.stringify(res));
        }
    }
}
