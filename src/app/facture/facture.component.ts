import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FactureService } from '../_services/facture.service';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-facture',
  templateUrl: './facture.component.html',
  styleUrls: ['./facture.component.css']
})
export class FactureComponent implements OnInit {

  constructor(public factureSrv:FactureService,public alertService:AlertService) { }
  submitted=false;
  listPlat:any;
  typeFacture:any;
  ajout:boolean;
  statutAdminPlat:string;

  type = [];
  facture = new FormGroup({
    tache: new FormControl(),
    name: new FormControl(),
    id: new FormControl(),
    // type: new FormControl(''),
  });

  
  ngOnInit(): void {
    this.statutAdminPlat = 'Création';
    this.ajout = true;
    this.factureSrv.getAll()
    .then(data => {
      this.listPlat = data
      this.updatetypeFacture('init');
    })
  }
  
  onCheckChange(event){
    if(event.target.checked){
      // Add a new control in the arrayForm
      this.type.push(event.target.value)
      event.target.ischecked = true
    }
    /* unselected */
    else{
      // find the unselected element
      this.type.splice(this.type.indexOf(event.target.checked), 1);
      event.target.ischecked = false
    }
  }
  onDelete(id){
    console.log('on remove',id);
    this.factureSrv.remove(id).then(res => {
      // console.log('error',res)
      if (res.status == '500' || res.status == '401') {
          console.log('error')
          this.submitted = false;
          this.alertService.error('erreur');
      } else {
          this.submitted = true;
          this.alertService.success('OK');
          this.ngOnInit();
          // this.initForm()
      }
      
    });
  }
  updatetypeFacture(type){
    if (type === 'init') {
      this.typeFacture = [
        { value: 'Facture',ischecked:false},
        {value: 'Devis',ischecked:false},
      ];
    } else {
      this.typeFacture.forEach(element => {
        if (type.includes(element.value) ) {
          element.ischecked = true;
          this.type.push(element.value)
        }
      });
    }
    
  }
  onUpdate(id) {
    this.ajout = false;
    this.statutAdminPlat = 'Modification'
    console.log(id);
    console.log(this.listPlat);
    this.listPlat.forEach(element => {
      if (element._id === id) {
        console.log(element);
        this.facture = new FormGroup({
          tache: new FormControl(element.html),
          name: new FormControl(element.name),
          id: new FormControl(),

          // type: new FormControl(''),
          
        });
        this.updatetypeFacture(element.type)

      }
    });

    
  }
  onSubmit(){
    if (this.ajout) {
      this.facture.value.type = this.type;
      console.warn(this.facture.value);
      this.factureSrv.add(this.facture.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    } else {
      this.facture.value.type = this.type;
      console.warn(this.facture.value);
      this.factureSrv.update(this.facture.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    }
    // console.warn(this.facture.value);
    
    
  }
  initForm(){
    this.facture = new FormGroup({
      tache: new FormControl(),
      id: new FormControl(),
      name: new FormControl(),
      

      // type: new FormControl(''),
    });
    // ici on reinitialise la liste de checkbox 

    
    this.type = [];


    this.ngOnInit();
  }
}