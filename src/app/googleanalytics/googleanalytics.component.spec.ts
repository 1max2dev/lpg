import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleanalyticsComponent } from './googleanalytics.component';

describe('GoogleanalyticsComponent', () => {
  let component: GoogleanalyticsComponent;
  let fixture: ComponentFixture<GoogleanalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoogleanalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleanalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
