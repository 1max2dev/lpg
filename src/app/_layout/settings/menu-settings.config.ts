// Default menu settings configurations

export interface MenuItem {
  title: string;
  icon: string;
  page: string;
  isExternalLink?: boolean;
  issupportExternalLink?: boolean;
  badge: { type: string, value: string };
  submenu: {
    items: Partial<MenuItem>[];
  };
  section: string;
}

export interface MenuConfig {
  horizontal_menu: {
    items: Partial<MenuItem>[]
  };
  vertical_menu: {
    items: Partial<MenuItem>[]
  };
}

export const MenuSettingsConfig: MenuConfig = {
  horizontal_menu: {
    items: [
      // {
      //   title: 'Changelog',
      //   icon: 'la-file',
      //   page: '/changelog',
      //   badge: { type: 'badge-danger', value: '2.1' }
      // },
      // {
      //   title: 'Templates',
      //   icon: 'la-television',
      //   page: 'null',
      //   submenu: {
      //     items: [
      //       {
      //         title: 'Horizontal',
      //         page: 'null'
      //       },
      //       {
      //         title: 'Vertical',
      //         page: 'null'
      //       },
      //     ]
      //   }
      // },
      {
        title: 'Les plats',
        icon: 'la-support',
        page: '/plat',
        isExternalLink: false
      },
      {
        title: 'Les menus',
        icon: 'la-support',
        page: '/menu',
        isExternalLink: false
      },
      {
        title: 'Les textes',
        icon: 'la-text-height',
        page: '/les-textes',
        isExternalLink: false,
      },
      {
        title: 'Google analytics',
        icon: 'la-text-height',
        page: '/ga',
        isExternalLink: false,
      }
    ]
  },
  vertical_menu: {
    items: [
      // {
      //   title: 'Changelog',
      //   icon: 'la-file',
      //   page: '/changelog',
      //   badge: { type: 'badge-danger', value: '2.1' }
      // },
      {
        title: 'Les factures',
        icon: 'la-television',
        page: 'null',
        submenu: {
          items: [
            {
              title: 'Gestions des factures',
              icon: 'la-support',
              page: '/facture',
              isExternalLink: false
            },
            {
              title: 'Gestions des taches',
              icon: 'la-support',
              page: '/tache'
            },
          ]
        }
      },
      { section: 'SUPPORT', icon: 'la-ellipsis-h' },
      {
        title: 'Les plats',
        icon: 'la-support',
        page: '/plat',
        isExternalLink: false
      },
      {
        title: 'Les menus',
        icon: 'la-support',
        page: '/menu',
        isExternalLink: false
      },
      {
        title: 'Les textes',
        icon: 'la-text-height',
        page: '/texte',
        isExternalLink: false,
      },
      {
        title: 'Google analytics',
        icon: 'la-text-height',
        page: '/ga',
        isExternalLink: false,
      }
    ]
  }

};





