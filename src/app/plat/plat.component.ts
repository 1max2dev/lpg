import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PlatService } from '../_services/plat.service';
import { AlertService } from '../_services/alert.service';
@Component({
  selector: 'app-plat',
  templateUrl: './plat.component.html',
  styleUrls: ['./plat.component.css']
})
export class PlatComponent implements OnInit {

  constructor(public platSrv:PlatService,public alertService:AlertService) { }
  submitted=false;
  listPlat:any;
  typePlat:any;
  ajout:boolean;
  statutAdminPlat:string;

  type = [];
  menu = new FormGroup({
    name: new FormControl(''),
    id:new FormControl(''),
    description: new FormControl(''),
    price: new FormControl(''),
    dispo: new FormControl(''),
    // type: new FormControl(''),
  });

  
  ngOnInit(): void {
    this.statutAdminPlat = 'Ajout';
    this.ajout = true;
    this.platSrv.getAll()
    .then(data => {
      this.listPlat = data
      this.updatetypePlat('init');
    })
  }
  
  onCheckChange(event){
    if(event.target.checked){
      // Add a new control in the arrayForm
      this.type.push(event.target.value)
      event.target.ischecked = true
    }
    /* unselected */
    else{
      // find the unselected element
      this.type.splice(this.type.indexOf(event.target.checked), 1);
      event.target.ischecked = false
    }
  }
  onDelete(id){
    console.log('on remove',id);
    this.platSrv.remove(id).then(res => {
      // console.log('error',res)
      if (res.status == '500' || res.status == '401') {
          console.log('error')
          this.submitted = false;
          this.alertService.error('erreur');
      } else {
          this.submitted = true;
          this.alertService.success('OK');
          this.ngOnInit();
          // this.initForm()
      }
      
    });
  }
  updatetypePlat(type){
    if (type === 'init') {
      this.typePlat = [
        { value: 'Entree',ischecked:false},
        {value: 'Plat',ischecked:false},
        {value: 'Dessert',ischecked:false},
        { value: 'Vin',ischecked:false},
        { value: 'Boisson',ischecked:false}
      ];
    } else {
      this.typePlat.forEach(element => {
        if (type.includes(element.value) ) {
          element.ischecked = true;
          this.type.push(element.value)
        }
      });
    }
    
  }
  onUpdate(id) {
    this.ajout = false;
    this.statutAdminPlat = 'Modifier'
    console.log(id);
    console.log(this.listPlat);
    this.listPlat.forEach(element => {
      if (element._id === id) {
        console.log(element);
        this.menu = new FormGroup({
          name: new FormControl(element.name),
          description: new FormControl(element.description),
          price: new FormControl(element.price),
          dispo: new FormControl(element.dispo),
          id:new FormControl(element._id),
          // type: new FormControl(''),
          
        });
        this.updatetypePlat(element.type)

      }
    });

    
  }
  onSubmit(){
    if (this.ajout) {
      this.menu.value.type = this.type;
    // console.warn(this.menu.value);
      this.platSrv.add(this.menu.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    } else {
      this.menu.value.type = this.type;
      // console.warn(this.menu.value);
      this.platSrv.update(this.menu.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    }
    // console.warn(this.menu.value);
    
    
  }
  initForm(){
    this.menu = new FormGroup({
      name: new FormControl(''),
      description: new FormControl(''),
      price: new FormControl(''),
      dispo: new FormControl(''),
      // type: new FormControl(''),
    });
    // ici on reinitialise la liste de checkbox 

    
    this.type = [];


    this.ngOnInit();
  }
}
