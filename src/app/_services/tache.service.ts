import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../_services/user.service';

@Injectable({
  providedIn: 'root'
})
export class TacheService {

  apiUrl = environment.apiUrl;
  currentUser;
   
   constructor(private http: HttpClient, private UserSrv:UserService) { }

  
  getAll(){
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      // console.log(this.currentUser);
      this.http.post(this.apiUrl + 'tache/', this.currentUser).subscribe(({
        next: data => { 
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
  getOne(){
    
  }
  update(value){
    console.log('update',value);
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      console.log(this.currentUser.userId);
      let input = value;
      input.userId = this.currentUser.userId
      this.http.post(this.apiUrl + 'tache/update', input).subscribe(({
        next: data => {
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
  add(value){
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      console.log(this.currentUser.userId);
      let input = value;
      input.userId = this.currentUser.userId
      this.http.post(this.apiUrl + 'tache/add', input).subscribe(({
        next: data => {
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
  remove(id){
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      console.log(this.currentUser.userId);
      let input = {id:id};

      this.http.post(this.apiUrl + 'tache/remove', input).subscribe(({
        next: data => {
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
}
