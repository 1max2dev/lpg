import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }
  currentUser;
  apiUrl = environment.apiUrl;

  doAnonymousLogin() {
    // return this.afAuth.auth.signInAnonymously();
  }
  getUser(){
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    console.log('on a un localstorage')
    console.log(this.currentUser);
    return this.currentUser;
  }
}
