import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  constructor(
   public afAuth: AngularFireAuth,
   private http: HttpClient
 ) {}

 apiUrl = environment.apiUrl;

  doAnonymousLogin() {
    return this.afAuth.auth.signInAnonymously();
  }

  doFacebookLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doGitHubLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GithubAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doTwitterLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.TwitterAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doGoogleLogin() {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      // firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
      // .then(res => {
      //   resolve(res);
      // }, err => reject(err));
      const input = value;
      this.http.post(this.apiUrl + 'users/', input).subscribe(res => {
        console.log(res);
        // blabla.loguser(email, password)
        resolve(res);

      });
    });
  }

  doLogin(value) {
    return new Promise<any>((resolve, reject) => {
      // firebase.auth().signInWithEmailAndPassword(value.email, value.password)
      // .then(res => {
      //   resolve(res);
      // }, err => reject(err));
        const input = value;
        this.http.post(this.apiUrl + 'auth/login', input).subscribe(({
          next: data => {
            resolve(data);
          },
          error: error => {
              resolve(error);
          }})
        );
    });
  }

  doLogout() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        localStorage.removeItem('currentUser');
        this.afAuth.auth.signOut();
        resolve();
      } else {
        localStorage.removeItem('currentUser');
        reject();
      }
    });
  }


}
