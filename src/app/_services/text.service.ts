import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../_services/user.service';

@Injectable({
  providedIn: 'root'
})
export class TextService {
  apiUrl = environment.apiUrl;
  currentUser;

  constructor(private http: HttpClient, private UserSrv:UserService) { }

  getAll(){
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      // console.log(this.currentUser);
      this.http.post(this.apiUrl + 'text/', this.currentUser).subscribe(({
        next: data => { 
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
  update(value){
    console.log('update',value);
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      console.log(this.currentUser.userId);
      let input = value;
      input.userId = this.currentUser.userId
      this.http.post(this.apiUrl + 'text/update', input).subscribe(({
        next: data => {
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
  add(value){
    return new Promise<any>((resolve, reject) => {
      this.currentUser = this.UserSrv.getUser()
      console.log(this.currentUser.userId);
      let input = value;
      input.userId = this.currentUser.userId
      this.http.post(this.apiUrl + 'text/add', input).subscribe(({
        next: data => {
          resolve(data);
        },
        error: error => {
            resolve(error);
      }}))
    });
  }
}
