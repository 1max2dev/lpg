import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TacheService } from '../_services/tache.service';
import { AlertService } from '../_services/alert.service';

@Component({
  selector: 'app-tache',
  templateUrl: './tache.component.html',
  styleUrls: ['./tache.component.css']
})
export class TacheComponent implements OnInit {

  constructor(public tacheSrv:TacheService,public alertService:AlertService) { }
  submitted=false;
  ListTache:any;
  typePlat:any;
  ajout:boolean;
  statutAdminTache:string;

  tache = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    code: new FormControl(''),
    price: new FormControl(''),
    id: new FormControl(''),
    // type: new FormControl(''),
  });

  
  ngOnInit(): void {
    this.statutAdminTache = 'Ajout';
    this.ajout = true;
    this.tacheSrv.getAll()
    .then(data => {
      this.ListTache = data
    })
  }
  

  onDelete(id){
    console.log('on remove',id);
    this.tacheSrv.remove(id).then(res => {
      // console.log('error',res)
      if (res.status == '500' || res.status == '401') {
          console.log('error')
          this.submitted = false;
          this.alertService.error('erreur');
      } else {
          this.submitted = true;
          this.alertService.success('OK');
          this.ngOnInit();
          // this.initForm()
      }
      
    });
  }

  onUpdate(id) {
    this.ajout = false;
    this.statutAdminTache = 'Modifier'
    console.log(id);
    console.log(this.ListTache);
    this.ListTache.forEach(element => {
      if (element._id === id) {
        console.log(element);
        this.tache = new FormGroup({
          name: new FormControl(element.name),
          description: new FormControl(element.description),
          code: new FormControl(element.code),
          price: new FormControl(element.price),
          id:new FormControl(element._id),
          // type: new FormControl(''),
          
        });

      }
    });

    
  }
  onSubmit(){
    if (this.ajout) {
    // console.warn(this.tache.value);
      this.tacheSrv.add(this.tache.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    } else {
      // console.warn(this.tache.value);
      this.tacheSrv.update(this.tache.value).then(res => {
      // console.log('error',res)
        if (res.status == '500' || res.status == '401') {
            console.log('error')
            this.submitted = false;
            this.alertService.error('erreur');
        } else {
            this.submitted = true;
            this.alertService.success('OK');
            this.initForm()
        }
      });
    }
    // console.warn(this.tache.value);
    
    
  }
  initForm(){
    this.tache = new FormGroup({
      name: new FormControl(''),
      description: new FormControl(''),
      code: new FormControl(''),
      price: new FormControl(''),
      id: new FormControl(''),
      // type: new FormControl(''),
    });
    // ici on reinitialise la liste de checkbox 



    this.ngOnInit();
  }
}
