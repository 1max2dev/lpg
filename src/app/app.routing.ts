﻿import { Routes, RouterModule } from '@angular/router';
import { PublicLayoutComponent } from './_layout/public-layout/public-layout.component';
import { PrivateLayoutComponent } from './_layout/private-layout/private-layout.component';
import { AuthGuard } from './_guards/auth.guard';
import { RegisterComponent } from './register';
import { LoginComponent } from './login';
import { ChangelogComponent } from './changelog/changelog.component';
import { MenuComponent } from './menu/menu.component';
import { TexteComponent } from './texte/texte.component';
import { PlatComponent } from './plat/plat.component';
import { FactureComponent } from './facture/facture.component';
import { TacheComponent } from './tache/tache.component';
import { GoogleanalyticsComponent } from './googleanalytics/googleanalytics.component';

const appRoutes: Routes = [
  // Public layout
  {
    path: '',
    component: PublicLayoutComponent,
    children: [
      { path: 'register', component: RegisterComponent },
      { path: 'login', component: LoginComponent },
      { path: '', component: LoginComponent }
    ]
  },
  // Private layout
  {
    path: '',
    component: PrivateLayoutComponent,
    children: [
      { path: 'logout', component: LoginComponent, canActivate: [AuthGuard] },
      { path: 'changelog', component: ChangelogComponent, canActivate: [AuthGuard] },
      { path: 'texte', component: TexteComponent, canActivate: [AuthGuard] },
      { path: 'plat', component: PlatComponent, canActivate: [AuthGuard] },
      { path: 'menu', component: MenuComponent, canActivate: [AuthGuard] },
      { path: 'facture', component: FactureComponent, canActivate: [AuthGuard] },
      { path: 'tache', component: TacheComponent, canActivate: [AuthGuard] },
      { path: 'ga', component: GoogleanalyticsComponent, canActivate: [AuthGuard] },
    ],
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'changelog' }
];

export const routing = RouterModule.forRoot(appRoutes, { scrollOffset: [0, 0], scrollPositionRestoration: 'top' });
